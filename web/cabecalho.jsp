<%-- 
    Document   : cabecalho
    Created on : Sep 11, 2018, 9:21:13 PM
    Author     : Otávio
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<html>
    <p> Data: <%= new SimpleDateFormat("dd/MM/yyyy").format(new Date())%>  </p>
    <p> IP: <%= request.getRemoteAddr()%> </p>
    <head>
        <style>
            .container-fluid {
                padding: 50px;
            }
            .bg-3 {
                background-color: #f0f0f0;
                color: #000;
            }
            .bg-1 {
                background-color: #d4d4d4;
                color: #000;
            }
            p{
                font-size: 16px;
            }
            .anuncio { 
                font-family: sans-serif;
                font-weight: normal;
                font-size: 3em 
            }
        </style>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
