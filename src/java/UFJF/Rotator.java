/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UFJF;

/**
 *
 * @author Otávio
 */
public class Rotator {

    private String images[] = {
        "images/1.jpg",
        "images/2.jpg",
        "images/3.jpg"};

    private String links[] = {
        "http://www.globo.com",
        "http://www.yahoo.com",
        "http://www.terra.com.br"};

    private int selectedIndex = 0;

    // retorna o nome do arquivo de imagem ao anúncio atual
    public String getImage() {
        return images[selectedIndex];
    } // fim do método getImage 

    // retorna o URL ao site Web correspondente ao anúncio
    public String getLink() {
        return links[selectedIndex];
    } // fim do método getLink 

    // atualiza selectedIndex assim as próximas chamadas para getImage e
    // getLink retornam um anúncio diferente
    public void nextAd() {
        selectedIndex = (selectedIndex + 1) % images.length;
    } // fim do método nextAd   
}
