/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UFJF;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *
 * @author Otávio
 */
public class Listener implements ServletContextListener, HttpSessionListener {

    ServletContext servletContext;
    static int counter = 0;

    public void contextInitialized(ServletContextEvent sce) {
        servletContext = sce.getServletContext();
        servletContext.setAttribute("userCounter", Integer.toString(counter));
    }

    public void contextDestroyed(ServletContextEvent sce) {
        // trata o evento
    }

    public void sessionCreated(HttpSessionEvent hse) {
        // trata o evento
        counter++;
    }

    public void sessionDestroyed(HttpSessionEvent hse) {
// trata o evento
        counter--;
    }

    public static int numeroSessao() {
        return counter;
    }
}
